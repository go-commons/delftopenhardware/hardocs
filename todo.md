## Week 34

- [x] Fix environment variable issue with "base" in config.js file vuepress
- [x] Fixed issue of loading global module in local module with node. Solution:

```js
// this is poiniting to the hardocs modules globally installed
const sidebarArray = require('hardocs/src/buildSidebar');
```

- [ ] What about git repo, if git repo doesn't exist ideally CLI should ask the user to create a repo. But the best is to not instal repo automatically, becaus it might be that user already has a git repo running in the folder.

## Week 33

- [x] Update readme with proper installation guides
- [x] Create a node module to create the repository structure,
      Currently this script is directly in the config file, is not modular, and therefore when pacakge is updated it will overwrite directly on the config file.
  - Add the image script used in Leilas journal inside this module mentioned.

## Notes with regard to command line structure

cli -
|- bin - cli
|- lib - main.js - cli.js
|- src
|- coffee
|- lib - main.coffee - cli.coffee
|- test - test.coffee
|- test - test.js
 — .gitignore
 — index.js
 — gulpfile.js
 — package.json
 — Makefile
 — README.md
