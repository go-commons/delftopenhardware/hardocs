#!/usr/bin/env node

/*
  takes the docs template from hardocs package installed globally and copies the template files and folder structure at current directory
  
*/

const fs = require('fs');
var path = require('path');

// set global appRoot from node_module installed globally
global.appRoot = path.resolve(__dirname);

const CURR_DIR = process.cwd();

function createDirectoryContents(templatePath, newProjectPath) {
    const filesToCreate = fs.readdirSync(templatePath);

    filesToCreate.forEach(file => {
        const origFilePath = `${templatePath}/${file}`;

        // get stats about the current file
        const stats = fs.statSync(origFilePath);

        if (stats.isFile()) {
            const contents = fs.readFileSync(origFilePath, 'utf8');

            const writePath = `${CURR_DIR}/${newProjectPath}/${file}`;
            fs.writeFileSync(writePath, contents, 'utf8');
        } else if (stats.isDirectory()) {
            fs.mkdirSync(`${CURR_DIR}/${newProjectPath}/${file}`);

            // recursive call
            createDirectoryContents(`${templatePath}/${file}`, `${newProjectPath}/${file}`);
        }
    });

}

/*
    Point to node environment variable to get the proper path to the templates!

*/
module.exports = createDirectoryContents(`${appRoot}/templates/project/`, `/`);

console.log(`Sucess with your project! hardocs template was installed at ${CURR_DIR}`)
