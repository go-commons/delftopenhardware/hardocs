const fs = require('fs');
const jsonToMarkdown2 = require('json-to-markdown-table2');
let checkdir = require('./check-dir.js');

checkdir.check_data();
checkdir.check_docs();

const args = process.argv.slice(2);
const [path] = args;

function docBom(path) {
    if (path == undefined) {
        if (checkdir.check_src('/bom/bom.csv')) {
            // Assumes that the file is in src/bom/bom.md
            path = 'src/bom/bom';
        } else {
            console.log('Place your csv file in src/bom/ or specify the path when using the command')
            process.exit();
        }

    }
    // Read csv file
    const csv = fs.readFileSync(path + '.csv', 'utf8');

    // gets csv and turns it into json
    function CSVToArray(strData, strDelimiter) {
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");
        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp((
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData = [
            []
        ];
        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches = null;
        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        while (arrMatches = objPattern.exec(strData)) {
            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[1];
            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push([]);
            }
            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[2]) {
                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                var strMatchedValue = arrMatches[2].replace(
                    new RegExp("\"\"", "g"), "\"");
            } else {
                // We found a non-quoted value.
                var strMatchedValue = arrMatches[3];
            }
            // Now that we have our value string, let's add
            // it to the data array.
            arrData[arrData.length - 1].push(strMatchedValue);
        }
        // Return the parsed data.
        return (arrData);
    }

    function CSV2JSON(csv) {
        var array = CSVToArray(csv);
        var objArray = [];
        for (var i = 1; i < array.length; i++) {
            objArray[i - 1] = {};
            for (var k = 0; k < array[0].length && k < array[i].length; k++) {
                var key = array[0][k];
                objArray[i - 1][key] = array[i][k]
            }
        }

        var json = JSON.stringify(objArray);
        var str = json.replace(/},/g, "},\r\n");

        return str;
    }

    let csv2json = CSV2JSON(csv);

    // This will make the data directory if it doesn't exists

    try {
        if (!fs.existsSync("data")) {
            fs.mkdirSync("data")
        }
    } catch (err) {
        console.error(err)
    }
    // Get json file to create table
    csv2json = JSON.parse(csv2json);

    // Makes an array of objects from the json file
    var result = Object.keys(csv2json).map(function(key) {
        return csv2json[key];
    });

    // transforms json file to create table
    var table = result.map(function(key) {
        let res = {}
        res.Part = key['Part Name'];
        res.Quantity = key.Quantity;
        res.Description = key.Description;
        //res.Link = key.Link;
        if (key.Image) {
            // Creates an image in markdown with a cetain size
            key.Image = `<img src="${key.Image}" alt="drawing" width="200"/>`;
            res.Image = key.Image;
        }
        return res;
    });



    // Creates the markdown table string
    const tabled = jsonToMarkdown2(table);

    // Stores the table in a file 
    fs.writeFileSync("./docs/Source/bom.md", tabled, function(err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });

    console.log("Built bom.md file in docs/source/ directory");
}

// let bompath = 'bom.csv';
// docBom('bom.csv');
module.exports.docBom = docBom(path);
