const http = require('https');
const fs = require('fs');

const obj = {
    1: 'https://images-na.ssl-images-amazon.com/images/I/41WzWE5uF5L.jpg',
    2: 'https://media.rs-online.com/t_large/F7816811-01.jpg',
    3: 'https://www.makerlab-electronics.com/my_uploads/2016/08/arduino-nano-1.jpg'
}

const download = function(name, path) {
    const file = fs.createWriteStream(`src/${name}.jpg`);
    const request = http.get(path, function(response) {
        response.pipe(file);
    });
}


var entries = Object.entries(obj);
for (const [name, link] of entries) {
    download(name, link);
}
