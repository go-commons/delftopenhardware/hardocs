#! /usr/bin/env node

const yaml = require('js-yaml');
const fs = require('fs');
const jsonToMarkdown2 = require('json-to-markdown-table2');


// Build data folder if it doesn't exist
try {
    if (!fs.existsSync("data")) {
        fs.mkdirSync("data")
    }
} catch (err) {
    console.error(err)
}

fs.writeFileSync("data/project.json", JSON.stringify(doc, undefined, '\t'), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});

// Get json file to create table
const file = JSON.parse(fs.readFileSync("data/project.json", "utf8"));

// Makes an array of objects from the json file
var result = Object.keys(file).map(function(key) {
    return file[key];
});

// Make a function that only captures the minimum elements to make the simple BOM


// transorms json file to create table
var table = result.map(function(key) {
    delete key.Suppliers
    delete key.Specs
    if (key.Image) {
        // Creates an image in markdown with a cetain size
        key.Image = `<img src="${key.Image}" alt="drawing" width="200" max-height="200"/>`;
    }
    return key;
});

// Creates the markdown table string
const tabled = jsonToMarkdown2(table);

// Stores the table in a file 
fs.writeFileSync("data/bom.md", tabled, function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});

console.log("built bom!");
