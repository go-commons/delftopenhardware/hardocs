# Introduce your project
Write the background of this project, key motivations and rationale behind it. Try to keep it short but comprehensive. Use also emojis and an image to show something about the project. 

## Name your product-hardware
Specify what it does, what is the main function, what is the context of use. Then introduce the specifics of this project and the main goals.

## Product specifications
Make a short list of key specifications. Try to make it as 
1. Spec 1
2. Spec 2
3. Spec 3
