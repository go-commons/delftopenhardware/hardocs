const fs = require('fs');

// This script is going to be the first to run and
// Build data folder if it doesn't exist
function check_data() {
    try {
        if (!fs.existsSync("data")) {
            fs.mkdirSync("data")
        }
    } catch (err) {
        console.error(err)
    }
}

function check_docs() {
    try {
        if (!fs.existsSync("docs/source/")) {
            console.log('Create docs/source/ to store the bom.md.');
            process.exit();
        }
    } catch (err) {
        console.error(err)
    }
}

function check_src(path) {
    try {
        // Specific subdirectories are specfified when the function is called in its context
        if (!fs.existsSync("src" + path)) {
            // fs.mkdirSync("src")
            return false;
        } else {
            return true;
        }

    } catch (err) {
        console.error(err)
    }
}

console.log('Checked folders');

module.exports.check_docs = check_docs;
module.exports.check_data = check_data;
module.exports.check_src = check_src;
