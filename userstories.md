git Value proposition:
- Easy made product lifecycle management with automation services that create and update product metadata dynamically.

Epic:
- As a harwdare developer I would like to easily create and maintain easily product design documentation. 
- I would like to publish also those designs, its changes and updates in new releases.
- I would like to reuse documented components and parts 

- As IT platform designer I would like users to be able to easily find related harwdare projects based on menaingful information such as ammount of usage and downloads, components being used, etc...

User stories:
- Document easily a designs data to contextualize the design being published
- Reuse parts-components being documented
- Update new fields information related to parts
- Present this data in various formats like bill of materials,
- Offerings, 
- I woul